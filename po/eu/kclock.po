# Translation for kclock.po to Euskara/Basque (eu).
# Copyright (C) 2020-2023 This file is copyright:
# This file is distributed under the same license as the kclock package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Ander Elortondo <ander.elor@gmail.com>, 2020.
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-14 00:49+0000\n"
"PO-Revision-Date: 2023-02-15 07:00+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Ander Elortondo,Iñigo Salvador Azurmendi"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Ander.elor@gmail.com,xalba@ni.eus"

#: main.cpp:43
#, kde-format
msgid "Select opened page"
msgstr "Hautatu irekitako orrialdea"

#: main.cpp:68
#, kde-format
msgid "© 2020-2022 KDE Community"
msgstr "© 2020-2022 KDE Komunitatea"

#: main.cpp:70
#, kde-format
msgid "Devin Lin"
msgstr "Devin Lin"

#: main.cpp:71
#, kde-format
msgid "Han Young"
msgstr "Han Young"

#: qml/alarm/AlarmForm.qml:90
#, kde-format
msgid "Days to repeat:"
msgstr "Errepikatu beharreko egunak:"

#: qml/alarm/AlarmForm.qml:92
#, kde-format
msgid "Select Days to Repeat"
msgstr "Errepikatu beharreko egunak hautatu"

#: qml/alarm/AlarmForm.qml:115
#, kde-format
msgid "Alarm Name (optional):"
msgstr "Alarmaren izena (aukerakoa)"

#: qml/alarm/AlarmForm.qml:116
#, kde-format
msgid "Wake Up"
msgstr "Esnatu"

#: qml/alarm/AlarmForm.qml:125
#, kde-format
msgid "Ring Duration:"
msgstr "Txirrinaren iraupena:"

#: qml/alarm/AlarmForm.qml:128 qml/alarm/AlarmForm.qml:139
#, kde-format
msgid "None"
msgstr "Bat ere ez"

#: qml/alarm/AlarmForm.qml:130 qml/alarm/AlarmForm.qml:140
#: qml/alarm/AlarmForm.qml:173 qml/alarm/AlarmForm.qml:177
#: qml/timer/TimerComponent.qml:149
#, kde-format
msgid "1 minute"
msgstr "minutu 1"

#: qml/alarm/AlarmForm.qml:132 qml/alarm/AlarmForm.qml:173
#, kde-format
msgid "%1 minutes"
msgstr "%1 minutu"

#: qml/alarm/AlarmForm.qml:135
#, kde-format
msgid "Select Ring Duration"
msgstr "Aukeratu txirrinaren iraupena"

#: qml/alarm/AlarmForm.qml:141 qml/alarm/AlarmForm.qml:178
#, kde-format
msgid "2 minutes"
msgstr "2 minutu"

#: qml/alarm/AlarmForm.qml:142 qml/alarm/AlarmForm.qml:179
#, kde-format
msgid "5 minutes"
msgstr "5 minutu"

#: qml/alarm/AlarmForm.qml:143 qml/alarm/AlarmForm.qml:180
#, kde-format
msgid "10 minutes"
msgstr "10 minutu"

#: qml/alarm/AlarmForm.qml:144 qml/alarm/AlarmForm.qml:181
#, kde-format
msgid "15 minutes"
msgstr "15 minutu"

#: qml/alarm/AlarmForm.qml:145 qml/alarm/AlarmForm.qml:182
#, kde-format
msgid "30 minutes"
msgstr "30 minutu"

#: qml/alarm/AlarmForm.qml:146 qml/alarm/AlarmForm.qml:183
#, kde-format
msgid "1 hour"
msgstr "ordu 1"

#: qml/alarm/AlarmForm.qml:171
#, kde-format
msgid "Snooze Length:"
msgstr "Kuluxkaren iraupena:"

#: qml/alarm/AlarmForm.qml:172
#, kde-format
msgid "Select Snooze Length"
msgstr "Aukeratu kuluxkaren iraupena"

#: qml/alarm/AlarmForm.qml:208
#, kde-format
msgid "Ring Sound:"
msgstr "Txirrinaren hotsa:"

#: qml/alarm/AlarmForm.qml:214
#, kde-format
msgid "Default Sound"
msgstr "Soinu lehenetsia"

#: qml/alarm/AlarmFormPage.qml:27 qml/alarm/AlarmRingingPopup.qml:45
#, kde-format
msgid "Alarm"
msgstr "Alarma"

#: qml/alarm/AlarmFormPage.qml:27
#, kde-format
msgctxt "Edit alarm page title"
msgid "Editing %1"
msgstr "%1 editatzea"

#: qml/alarm/AlarmFormPage.qml:28 qml/alarm/AlarmListPage.qml:30
#, kde-format
msgid "New Alarm"
msgstr "Alarma berria"

#: qml/alarm/AlarmFormPage.qml:40 qml/alarm/AlarmFormPage.qml:63
#: qml/timer/TimerFormWrapper.qml:85 qml/timer/TimerFormWrapper.qml:144
#, kde-format
msgid "Done"
msgstr "Eginda"

#: qml/alarm/AlarmFormPage.qml:69 qml/timer/TimerFormWrapper.qml:80
#: qml/timer/TimerFormWrapper.qml:139
#, kde-format
msgid "Cancel"
msgstr "Utzi"

#: qml/alarm/AlarmListDelegate.qml:88
#, kde-format
msgid "Snoozed %1 minute"
msgid_plural "Snoozed %1 minutes"
msgstr[0] "Minutu %1eko kuluxka egin du"
msgstr[1] "%1 minutuko kuluxka egin du"

#: qml/alarm/AlarmListDelegate.qml:112 qml/time/TimePageDelegate.qml:46
#: qml/timer/TimerListDelegate.qml:176 qml/timer/TimerPage.qml:60
#: qml/timer/TimerPage.qml:109
#, kde-format
msgid "Delete"
msgstr "Ezabatu"

#: qml/alarm/AlarmListPage.qml:25 qml/components/BottomToolbar.qml:115
#: qml/components/Sidebar.qml:108
#, kde-format
msgid "Alarms"
msgstr "Alarmak"

#: qml/alarm/AlarmListPage.qml:38 qml/time/TimePage.qml:37
#, kde-format
msgid "Edit"
msgstr "Editatu"

#: qml/alarm/AlarmListPage.qml:47 qml/components/Sidebar.qml:134
#: qml/settings/SettingsPage.qml:21 qml/stopwatch/StopwatchPage.qml:70
#: qml/time/TimePage.qml:60 qml/timer/TimerListPage.qml:37
#, kde-format
msgid "Settings"
msgstr "Ezarpenak"

#: qml/alarm/AlarmListPage.qml:59
#, kde-format
msgid ""
"The clock daemon was not found. Please start kclockd in order to have alarm "
"functionality."
msgstr ""
"Ez du aurkitu erlojuaren daimonik. Mesedez abiarazi «kclockd», alarma "
"funtzionaltasuna izateko."

#: qml/alarm/AlarmListPage.qml:82
#, kde-format
msgid "No alarms configured"
msgstr "Ez da alarmarik konfiguratu"

#: qml/alarm/AlarmListPage.qml:87
#, kde-format
msgid "Add alarm"
msgstr "Gehitu alarma"

#: qml/alarm/AlarmListPage.qml:120
#, kde-format
msgid "alarm"
msgstr "alarma"

#: qml/alarm/AlarmListPage.qml:120
#, kde-format
msgid "Deleted %1"
msgstr "Ezabatu %1"

#: qml/alarm/AlarmRingingPopup.qml:27
#, kde-format
msgid "Alarm is ringing"
msgstr "Alarma jotzen ari da"

#: qml/alarm/AlarmRingingPopup.qml:55
#, kde-format
msgid "Snooze"
msgstr "Kuluxka"

#: qml/alarm/AlarmRingingPopup.qml:59 qml/timer/TimerRingingPopup.qml:56
#, kde-format
msgid "Dismiss"
msgstr "Baztertu"

#: qml/alarm/SoundPickerPage.qml:23
#, kde-format
msgid "Select Alarm Sound"
msgstr "Aukeratu alarmaren soinua"

#: qml/alarm/SoundPickerPage.qml:60
#, kde-format
msgid "Select from files…"
msgstr "Hautatu fitxategietatik..."

#: qml/alarm/SoundPickerPage.qml:71
#, kde-format
msgid "Default"
msgstr "Lehenetsia"

#: qml/alarm/SoundPickerPage.qml:134
#, kde-format
msgid "Choose an audio"
msgstr "Hautatu audio bat"

#: qml/alarm/SoundPickerPage.qml:142
#, kde-format
msgid "Audio files (*.wav *.mp3 *.ogg *.aac *.flac *.webm *.mka *.opus)"
msgstr "Audio fitxategiak (*.wav *.mp3 *.ogg *.aac *.flac *.webm *.mka *.opus)"

#: qml/alarm/SoundPickerPage.qml:142
#, kde-format
msgid "All files (*)"
msgstr "Fitxategi guztiak (*)"

#: qml/components/BottomToolbar.qml:85 qml/components/Sidebar.qml:54
#: qml/time/TimePage.qml:25
#, kde-format
msgid "Time"
msgstr "Ordua"

#: qml/components/BottomToolbar.qml:95 qml/components/Sidebar.qml:72
#: qml/timer/TimerListPage.qml:21
#, kde-format
msgid "Timers"
msgstr "Tenporizadoreak"

#: qml/components/BottomToolbar.qml:105 qml/components/Sidebar.qml:90
#: qml/stopwatch/StopwatchPage.qml:23
#, kde-format
msgid "Stopwatch"
msgstr "Kronometroa"

#: qml/components/formatUtil.js:11
#, kde-format
msgid "Only once"
msgstr "Behin bakarrik"

#: qml/components/formatUtil.js:16
#, kde-format
msgid "Everyday"
msgstr "Egunero"

#: qml/components/formatUtil.js:19
#, kde-format
msgid "Weekdays"
msgstr "Astegunetan"

#: qml/components/formatUtil.js:22
#, kde-format
msgid "Mon, "
msgstr "Asl, "

#: qml/components/formatUtil.js:23
#, kde-format
msgid "Tue, "
msgstr "Asr, "

#: qml/components/formatUtil.js:24
#, kde-format
msgid "Wed, "
msgstr "Asz, "

#: qml/components/formatUtil.js:25
#, kde-format
msgid "Thu, "
msgstr "Osg, "

#: qml/components/formatUtil.js:26
#, kde-format
msgid "Fri, "
msgstr "Osr, "

#: qml/components/formatUtil.js:27
#, kde-format
msgid "Sat, "
msgstr "Lar, "

#: qml/components/formatUtil.js:28
#, kde-format
msgid "Sun, "
msgstr "Iga, "

#: qml/components/TimePicker.qml:70
#, kde-format
msgid "AM"
msgstr "AM"

#: qml/components/TimePicker.qml:70
#, kde-format
msgid "PM"
msgstr "PM"

#: qml/main.qml:23
#, kde-format
msgid "Clock"
msgstr "Erlojua"

#: qml/settings/SettingsPage.qml:43
#, kde-format
msgid "Time Format:"
msgstr "Orduaren formatua:"

#: qml/settings/SettingsPage.qml:44
#, kde-format
msgid "Select Time Format"
msgstr "Aukeratu orduaren formatua:"

#: qml/settings/SettingsPage.qml:48 qml/settings/SettingsPage.qml:61
#, kde-format
msgid "Use System Default"
msgstr "Erabili sisteman lehenetsitakoa"

#: qml/settings/SettingsPage.qml:50 qml/settings/SettingsPage.qml:62
#, kde-format
msgid "12 Hour Time"
msgstr "12 orduko ordua"

#: qml/settings/SettingsPage.qml:52 qml/settings/SettingsPage.qml:63
#, kde-format
msgid "24 Hour Time"
msgstr "24 orduko ordua"

#: qml/settings/SettingsPage.qml:83
#, kde-format
msgid "More Info:"
msgstr "Informazio gehiago:"

#: qml/settings/SettingsPage.qml:84
#, kde-format
msgid "About"
msgstr "Honi buruz"

#: qml/stopwatch/StopwatchPage.qml:61 qml/stopwatch/StopwatchPage.qml:127
#: qml/timer/TimerListDelegate.qml:166 qml/timer/TimerPage.qml:54
#: qml/timer/TimerPage.qml:103
#, kde-format
msgid "Reset"
msgstr "Berrezarri"

#: qml/stopwatch/StopwatchPage.qml:127 qml/timer/TimerListDelegate.qml:156
#: qml/timer/TimerPage.qml:45 qml/timer/TimerPage.qml:97
#, kde-format
msgid "Pause"
msgstr "Eten"

#: qml/stopwatch/StopwatchPage.qml:127 qml/timer/TimerListDelegate.qml:156
#: qml/timer/TimerPage.qml:45 qml/timer/TimerPage.qml:97
#, kde-format
msgid "Start"
msgstr "Abiarazi"

#: qml/stopwatch/StopwatchPage.qml:145
#, kde-format
msgid "Lap"
msgstr "Itzulia"

#: qml/stopwatch/StopwatchPage.qml:238
#, kde-format
msgid "#%1"
msgstr "#%1"

#: qml/time/AddLocationListView.qml:52
#, kde-format
msgid "No locations found"
msgstr "Ez du kokalekurik aurkitu"

#: qml/time/AddLocationPage.qml:17 qml/time/AddLocationWrapper.qml:33
#, kde-format
msgid "Add Location"
msgstr "Gehitu kokalekua"

#: qml/time/TimePage.qml:50
#, kde-format
msgid "Add"
msgstr "Gehitu"

#: qml/time/TimePage.qml:152
#, kde-format
msgid "No locations configured"
msgstr "Ez da kokalekurik konfiguratu"

#: qml/timer/PresetDurationButton.qml:23 qml/timer/TimerForm.qml:39
#, kde-format
msgid "1 m"
msgstr "m 1"

#: qml/timer/TimerForm.qml:47
#, kde-format
msgid "5 m"
msgstr "5 m"

#: qml/timer/TimerForm.qml:55
#, kde-format
msgid "10 m"
msgstr "10 m"

#: qml/timer/TimerForm.qml:67
#, kde-format
msgid "15 m"
msgstr "15 m"

#: qml/timer/TimerForm.qml:75
#, kde-format
msgid "30 m"
msgstr "30 m"

#: qml/timer/TimerForm.qml:83
#, kde-format
msgid "1 h"
msgstr "ordu 1"

#: qml/timer/TimerForm.qml:95
#, kde-format
msgid "<b>Duration:</b>"
msgstr "<b>Iraupena:</b>"

#: qml/timer/TimerForm.qml:104
#, kde-format
msgid "hours"
msgstr "ordu"

#: qml/timer/TimerForm.qml:115
#, kde-format
msgid "minutes"
msgstr "minutu"

#: qml/timer/TimerForm.qml:126
#, kde-format
msgid "seconds"
msgstr "segundo"

#: qml/timer/TimerForm.qml:132
#, kde-format
msgid "<b>Label:</b>"
msgstr "<b>Etiketa</b>"

#: qml/timer/TimerForm.qml:133
#, kde-format
msgid "Timer"
msgstr "Tenporizadorea"

#: qml/timer/TimerForm.qml:138
#, kde-format
msgid "<b>Command at timeout:</b>"
msgstr "<b>Denbora-muga betetzean, komandoa:</b>"

#: qml/timer/TimerForm.qml:141
#, kde-format
msgid "optional"
msgstr "aukerakoa"

#: qml/timer/TimerForm.qml:157
#, kde-format
msgid "Hide Presets"
msgstr "Ezkutatu aurre-ezarpenak"

#: qml/timer/TimerForm.qml:157
#, kde-format
msgid "Show Presets"
msgstr "Erakutsi aurre-ezarpenak"

#: qml/timer/TimerForm.qml:162
#, kde-format
msgid "Toggle Delete"
msgstr "Txandakatu ezabatzea"

#: qml/timer/TimerFormWrapper.qml:55
#, kde-format
msgid "<b>Create New Timer</b>"
msgstr "<b>Sortu tenporizadore berria</b>"

#: qml/timer/TimerFormWrapper.qml:73 qml/timer/TimerFormWrapper.qml:132
#, kde-format
msgid "Save As Preset"
msgstr "Gorde aurre-ezarpen gisa"

#: qml/timer/TimerFormWrapper.qml:99
#, kde-format
msgid "Create timer"
msgstr "Sortu tenporizadorea"

#: qml/timer/TimerListDelegate.qml:145 qml/timer/TimerPage.qml:69
#: qml/timer/TimerPage.qml:118
#, kde-format
msgid "Loop Timer"
msgstr "Tenporizadore begizta"

#: qml/timer/TimerListPage.qml:27
#, kde-format
msgid "New Timer"
msgstr "Tenporizadore berria"

#: qml/timer/TimerListPage.qml:50
#, kde-format
msgid ""
"The clock daemon was not found. Please start kclockd in order to have timer "
"functionality."
msgstr ""
"Ez du aurkitu erlojuaren daimona. Mesedez abiarazi «kclockd», tenporizadore "
"funtzionaltasuna izateko."

#: qml/timer/TimerListPage.qml:84
#, kde-format
msgid "No timers configured"
msgstr "Ez da tenporizadorerik konfiguratu"

#: qml/timer/TimerListPage.qml:89
#, kde-format
msgid "Add timer"
msgstr "Gehitu tenporizadorea"

#: qml/timer/TimerPage.qml:24
#, kde-format
msgid "New timer"
msgstr "Tenporizadore berria"

#: qml/timer/TimerRingingPopup.qml:27
#, kde-format
msgid "Timer has finished"
msgstr "Tenporizadoreak bukatu du"

#: qml/timer/TimerRingingPopup.qml:46
#, kde-format
msgid "%1 has completed."
msgstr "%1 osatu da."

#: qml/timer/TimerRingingPopup.qml:46
#, kde-format
msgid "The timer has completed."
msgstr "Tenporizadoreak bukatu du."

#: savedlocationsmodel.cpp:106
#, kde-format
msgid "%1 hour and 30 minutes ahead"
msgstr "ordu %1 eta 30 minutuko aurrerapena"

#: savedlocationsmodel.cpp:106
#, kde-format
msgid "%1 hours and 30 minutes ahead"
msgstr "%1 ordu eta 30 minutuko aurrerapena"

#: savedlocationsmodel.cpp:108
#, kde-format
msgid "%1 hour ahead"
msgstr "Ordu %1(e)ko aurrerapena"

#: savedlocationsmodel.cpp:108
#, kde-format
msgid "%1 hours ahead"
msgstr "%1 ordukoko aurrerapena"

#: savedlocationsmodel.cpp:113
#, kde-format
msgid "%1 hour and 30 minutes behind"
msgstr "Ordu %1 eta erdiko atzerapena"

#: savedlocationsmodel.cpp:113
#, kde-format
msgid "%1 hours and 30 minutes behind"
msgstr "%1 ordu eta 30 minutuko atzerapena"

#: savedlocationsmodel.cpp:115
#, kde-format
msgid "%1 hour behind"
msgstr "Ordu %1(e)ko atzerapena"

#: savedlocationsmodel.cpp:115
#, kde-format
msgid "%1 hours behind"
msgstr "%1 orduko atzerapena"

#: savedlocationsmodel.cpp:118
#, kde-format
msgid "Local time"
msgstr "Ordu lokala"

#: utilmodel.cpp:72
#, kde-format
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "eguna %1"
msgstr[1] "%1 egun"

#: utilmodel.cpp:76
#, kde-format
msgid ", "
msgstr ", "

#: utilmodel.cpp:78 utilmodel.cpp:84
#, kde-format
msgid " and "
msgstr "eta"

#: utilmodel.cpp:80
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "ordu %1"
msgstr[1] "%1 ordu"

#: utilmodel.cpp:86
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "minutu %1"
msgstr[1] "%1 minutu"

#: utilmodel.cpp:90
#, kde-format
msgid "Alarm will ring in under a minute"
msgstr "Alarma minutu bat baina gutxiagoz joko du"

#: utilmodel.cpp:92
#, kde-format
msgid "Alarm will ring in %1"
msgstr "Alarmak %1(e)an joko du"

#~ msgid "Snoozed %1 minutes"
#~ msgstr "%1 minutuko kuluxka"

#~ msgid "View"
#~ msgstr "Ikuspegia"

#~ msgctxt "@info"
#~ msgid "Alarm: <shortcut>%1 %2</shortcut>"
#~ msgstr "Alarma: <shortcut>%1 %2</shortcut>"

#~ msgid "Don't use PowerDevil for alarms if it is available"
#~ msgstr "Erabilgarri badago, ez erabili PowerDevil alarmetarako"

#~ msgid ""
#~ "Allow the clock process to be in the background and launched on startup."
#~ msgstr ""
#~ "Baimendu erloju prozesua atzeko planoan egoten eta abioan abiaraztea."

#~ msgid "A mobile friendly clock app built with Kirigami."
#~ msgstr ""
#~ "Mugikorrekiko lagunkoia den erloju aplikazio bat, Kirigami erabiliz "
#~ "eraikia."

#~ msgid "© 2020 Plasma Development Team"
#~ msgstr "© 2020 Plasma garapen taldea"

#~ msgid "Alarm Volume"
#~ msgstr "Alarmaren bolumena"

#~ msgid "Silence Alarm After"
#~ msgstr "Isilarazi alarma hau igarotakoan,"

#~ msgid "Change Alarm Volume"
#~ msgstr "Aldatu alarmaren bolumena"

#~ msgid "Volume: "
#~ msgstr "Bolumena: "

#~ msgid "Search for a city"
#~ msgstr "Hiri bat bilatzea"

#~ msgid "%1 %2"
#~ msgstr "%1 %2"

#, fuzzy
#~| msgid "Timezones"
#~ msgid "<b>Timezones</b>"
#~ msgstr "Ordu-eremuak"

#~ msgid "Timezones"
#~ msgstr "Ordu-eremuak"

#~ msgid "30 seconds"
#~ msgstr "30 segundo"

#~ msgid "Never"
#~ msgstr "Inoiz ez"

#~ msgid "3 minutes"
#~ msgstr "3 minutu"

#~ msgid "4 minutes"
#~ msgstr "4 minutu"

#~ msgid "hour"
#~ msgstr "ordu"

#~ msgid "Alarm Name"
#~ msgstr "Alarmaren izena"

#~ msgid "Create"
#~ msgstr "Sortu"

#~ msgid "Lap %1"
#~ msgstr "%1. itzulia"

#~ msgid " minutes"
#~ msgstr " minutuz"

#~ msgid "Run in background mode"
#~ msgstr "Exekutatu atzeko planoko moduan"

#, fuzzy
#~| msgid "hours"
#~ msgid " hours"
#~ msgstr "ordu"

#, fuzzy
#~| msgid "hours"
#~ msgid "hours "
#~ msgstr "ordu"

#, fuzzy
#~| msgid "hour"
#~ msgid "hour "
#~ msgstr "ordu"

#, fuzzy
#~| msgid "minutes"
#~ msgid "minute"
#~ msgstr "minutu"

#~ msgid "Change Timer Duration"
#~ msgstr "Aldatu tenporizadore iraupena"

#~ msgid "Hours"
#~ msgstr "Orduak"

#~ msgid "Minutes"
#~ msgstr "Minutuak"

#~ msgid "Seconds"
#~ msgstr "Segundoak"

#~ msgid "Edit Alarm"
#~ msgstr "Editatu alarma"

#~ msgid "Start week on"
#~ msgstr "Aste hasiera"
