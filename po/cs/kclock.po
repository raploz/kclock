# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kclock package.
# Vit Pelcak <vit@pelcak.org>, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-14 00:49+0000\n"
"PO-Revision-Date: 2023-03-04 23:30+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: main.cpp:43
#, kde-format
msgid "Select opened page"
msgstr ""

#: main.cpp:68
#, kde-format
msgid "© 2020-2022 KDE Community"
msgstr "© 2020-2022 Komunita KDE"

#: main.cpp:70
#, kde-format
msgid "Devin Lin"
msgstr "Devin Lin"

#: main.cpp:71
#, kde-format
msgid "Han Young"
msgstr "Han Young"

#: qml/alarm/AlarmForm.qml:90
#, kde-format
msgid "Days to repeat:"
msgstr ""

#: qml/alarm/AlarmForm.qml:92
#, kde-format
msgid "Select Days to Repeat"
msgstr ""

#: qml/alarm/AlarmForm.qml:115
#, kde-format
msgid "Alarm Name (optional):"
msgstr ""

#: qml/alarm/AlarmForm.qml:116
#, kde-format
msgid "Wake Up"
msgstr "Probudit"

#: qml/alarm/AlarmForm.qml:125
#, kde-format
msgid "Ring Duration:"
msgstr "Doba trvání prstence:"

# žádné parametry funkce v inspektoru funkcí
#: qml/alarm/AlarmForm.qml:128 qml/alarm/AlarmForm.qml:139
#, kde-format
msgid "None"
msgstr "Nic"

#: qml/alarm/AlarmForm.qml:130 qml/alarm/AlarmForm.qml:140
#: qml/alarm/AlarmForm.qml:173 qml/alarm/AlarmForm.qml:177
#: qml/timer/TimerComponent.qml:149
#, kde-format
msgid "1 minute"
msgstr "1 minuta"

#: qml/alarm/AlarmForm.qml:132 qml/alarm/AlarmForm.qml:173
#, kde-format
msgid "%1 minutes"
msgstr "%1 minut"

#: qml/alarm/AlarmForm.qml:135
#, kde-format
msgid "Select Ring Duration"
msgstr ""

#: qml/alarm/AlarmForm.qml:141 qml/alarm/AlarmForm.qml:178
#, kde-format
msgid "2 minutes"
msgstr "2 minuty"

#: qml/alarm/AlarmForm.qml:142 qml/alarm/AlarmForm.qml:179
#, kde-format
msgid "5 minutes"
msgstr "5 minut"

#: qml/alarm/AlarmForm.qml:143 qml/alarm/AlarmForm.qml:180
#, kde-format
msgid "10 minutes"
msgstr "10 minut"

#: qml/alarm/AlarmForm.qml:144 qml/alarm/AlarmForm.qml:181
#, kde-format
msgid "15 minutes"
msgstr "15 minut"

#: qml/alarm/AlarmForm.qml:145 qml/alarm/AlarmForm.qml:182
#, kde-format
msgid "30 minutes"
msgstr "30 minut"

#: qml/alarm/AlarmForm.qml:146 qml/alarm/AlarmForm.qml:183
#, kde-format
msgid "1 hour"
msgstr "1 hodina"

#: qml/alarm/AlarmForm.qml:171
#, kde-format
msgid "Snooze Length:"
msgstr ""

#: qml/alarm/AlarmForm.qml:172
#, kde-format
msgid "Select Snooze Length"
msgstr ""

#: qml/alarm/AlarmForm.qml:208
#, kde-format
msgid "Ring Sound:"
msgstr ""

#: qml/alarm/AlarmForm.qml:214
#, kde-format
msgid "Default Sound"
msgstr "Výchozí zvuk"

#: qml/alarm/AlarmFormPage.qml:27 qml/alarm/AlarmRingingPopup.qml:45
#, kde-format
msgid "Alarm"
msgstr "Alarm"

#: qml/alarm/AlarmFormPage.qml:27
#, kde-format
msgctxt "Edit alarm page title"
msgid "Editing %1"
msgstr "Úprava %1"

#: qml/alarm/AlarmFormPage.qml:28 qml/alarm/AlarmListPage.qml:30
#, kde-format
msgid "New Alarm"
msgstr "&Nová upomínka"

#: qml/alarm/AlarmFormPage.qml:40 qml/alarm/AlarmFormPage.qml:63
#: qml/timer/TimerFormWrapper.qml:85 qml/timer/TimerFormWrapper.qml:144
#, kde-format
msgid "Done"
msgstr "Hotovo"

#: qml/alarm/AlarmFormPage.qml:69 qml/timer/TimerFormWrapper.qml:80
#: qml/timer/TimerFormWrapper.qml:139
#, kde-format
msgid "Cancel"
msgstr "Zrušit"

#: qml/alarm/AlarmListDelegate.qml:88
#, kde-format
msgid "Snoozed %1 minute"
msgid_plural "Snoozed %1 minutes"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: qml/alarm/AlarmListDelegate.qml:112 qml/time/TimePageDelegate.qml:46
#: qml/timer/TimerListDelegate.qml:176 qml/timer/TimerPage.qml:60
#: qml/timer/TimerPage.qml:109
#, kde-format
msgid "Delete"
msgstr "Smazat"

#: qml/alarm/AlarmListPage.qml:25 qml/components/BottomToolbar.qml:115
#: qml/components/Sidebar.qml:108
#, kde-format
msgid "Alarms"
msgstr "Alarmy"

#: qml/alarm/AlarmListPage.qml:38 qml/time/TimePage.qml:37
#, kde-format
msgid "Edit"
msgstr "Upravit"

#: qml/alarm/AlarmListPage.qml:47 qml/components/Sidebar.qml:134
#: qml/settings/SettingsPage.qml:21 qml/stopwatch/StopwatchPage.qml:70
#: qml/time/TimePage.qml:60 qml/timer/TimerListPage.qml:37
#, kde-format
msgid "Settings"
msgstr "Nastavení"

#: qml/alarm/AlarmListPage.qml:59
#, kde-format
msgid ""
"The clock daemon was not found. Please start kclockd in order to have alarm "
"functionality."
msgstr ""

#: qml/alarm/AlarmListPage.qml:82
#, kde-format
msgid "No alarms configured"
msgstr ""

#: qml/alarm/AlarmListPage.qml:87
#, kde-format
msgid "Add alarm"
msgstr "Přidat budík"

#: qml/alarm/AlarmListPage.qml:120
#, kde-format
msgid "alarm"
msgstr "Budík"

#: qml/alarm/AlarmListPage.qml:120
#, kde-format
msgid "Deleted %1"
msgstr "Smazáno %1"

#: qml/alarm/AlarmRingingPopup.qml:27
#, kde-format
msgid "Alarm is ringing"
msgstr ""

#: qml/alarm/AlarmRingingPopup.qml:55
#, kde-format
msgid "Snooze"
msgstr "Uspat"

#: qml/alarm/AlarmRingingPopup.qml:59 qml/timer/TimerRingingPopup.qml:56
#, kde-format
msgid "Dismiss"
msgstr "Zahodit"

#: qml/alarm/SoundPickerPage.qml:23
#, kde-format
msgid "Select Alarm Sound"
msgstr ""

#: qml/alarm/SoundPickerPage.qml:60
#, kde-format
msgid "Select from files…"
msgstr ""

#: qml/alarm/SoundPickerPage.qml:71
#, kde-format
msgid "Default"
msgstr "Výchozí"

#: qml/alarm/SoundPickerPage.qml:134
#, kde-format
msgid "Choose an audio"
msgstr ""

#: qml/alarm/SoundPickerPage.qml:142
#, kde-format
msgid "Audio files (*.wav *.mp3 *.ogg *.aac *.flac *.webm *.mka *.opus)"
msgstr "Audio soubory (*.wav *.mp3 *.ogg *.aac *.flac *.webm *.mka *.opus)"

#: qml/alarm/SoundPickerPage.qml:142
#, kde-format
msgid "All files (*)"
msgstr "Všechny soubory (*)"

#: qml/components/BottomToolbar.qml:85 qml/components/Sidebar.qml:54
#: qml/time/TimePage.qml:25
#, kde-format
msgid "Time"
msgstr "Čas"

#: qml/components/BottomToolbar.qml:95 qml/components/Sidebar.qml:72
#: qml/timer/TimerListPage.qml:21
#, kde-format
msgid "Timers"
msgstr "Časovače"

#: qml/components/BottomToolbar.qml:105 qml/components/Sidebar.qml:90
#: qml/stopwatch/StopwatchPage.qml:23
#, kde-format
msgid "Stopwatch"
msgstr "Stopky"

#: qml/components/formatUtil.js:11
#, kde-format
msgid "Only once"
msgstr "Pouze jednou"

#: qml/components/formatUtil.js:16
#, kde-format
msgid "Everyday"
msgstr "Každý den"

#: qml/components/formatUtil.js:19
#, kde-format
msgid "Weekdays"
msgstr "Dny týdne"

#: qml/components/formatUtil.js:22
#, kde-format
msgid "Mon, "
msgstr "Po, "

#: qml/components/formatUtil.js:23
#, kde-format
msgid "Tue, "
msgstr "Út, "

#: qml/components/formatUtil.js:24
#, kde-format
msgid "Wed, "
msgstr "St, "

#: qml/components/formatUtil.js:25
#, kde-format
msgid "Thu, "
msgstr "Čt, "

#: qml/components/formatUtil.js:26
#, kde-format
msgid "Fri, "
msgstr "Pá, "

#: qml/components/formatUtil.js:27
#, kde-format
msgid "Sat, "
msgstr "So, "

#: qml/components/formatUtil.js:28
#, kde-format
msgid "Sun, "
msgstr "Slunce, "

#: qml/components/TimePicker.qml:70
#, kde-format
msgid "AM"
msgstr "dop."

#: qml/components/TimePicker.qml:70
#, kde-format
msgid "PM"
msgstr "odp."

#: qml/main.qml:23
#, kde-format
msgid "Clock"
msgstr "Hodiny"

#: qml/settings/SettingsPage.qml:43
#, kde-format
msgid "Time Format:"
msgstr "Formát času:"

#: qml/settings/SettingsPage.qml:44
#, kde-format
msgid "Select Time Format"
msgstr ""

#: qml/settings/SettingsPage.qml:48 qml/settings/SettingsPage.qml:61
#, kde-format
msgid "Use System Default"
msgstr "Použít výchozí systémový"

#: qml/settings/SettingsPage.qml:50 qml/settings/SettingsPage.qml:62
#, kde-format
msgid "12 Hour Time"
msgstr ""

#: qml/settings/SettingsPage.qml:52 qml/settings/SettingsPage.qml:63
#, kde-format
msgid "24 Hour Time"
msgstr ""

#: qml/settings/SettingsPage.qml:83
#, kde-format
msgid "More Info:"
msgstr "Více informací:"

#: qml/settings/SettingsPage.qml:84
#, kde-format
msgid "About"
msgstr "O aplikaci"

#: qml/stopwatch/StopwatchPage.qml:61 qml/stopwatch/StopwatchPage.qml:127
#: qml/timer/TimerListDelegate.qml:166 qml/timer/TimerPage.qml:54
#: qml/timer/TimerPage.qml:103
#, kde-format
msgid "Reset"
msgstr "Obnovit"

#: qml/stopwatch/StopwatchPage.qml:127 qml/timer/TimerListDelegate.qml:156
#: qml/timer/TimerPage.qml:45 qml/timer/TimerPage.qml:97
#, kde-format
msgid "Pause"
msgstr "Pozastavit"

#: qml/stopwatch/StopwatchPage.qml:127 qml/timer/TimerListDelegate.qml:156
#: qml/timer/TimerPage.qml:45 qml/timer/TimerPage.qml:97
#, kde-format
msgid "Start"
msgstr "Spustit"

#: qml/stopwatch/StopwatchPage.qml:145
#, kde-format
msgid "Lap"
msgstr ""

#: qml/stopwatch/StopwatchPage.qml:238
#, kde-format
msgid "#%1"
msgstr "#%1"

#: qml/time/AddLocationListView.qml:52
#, kde-format
msgid "No locations found"
msgstr "Nenalezeno žádné místo"

#: qml/time/AddLocationPage.qml:17 qml/time/AddLocationWrapper.qml:33
#, kde-format
msgid "Add Location"
msgstr "Přidat umístění"

#: qml/time/TimePage.qml:50
#, kde-format
msgid "Add"
msgstr "Přidat"

#: qml/time/TimePage.qml:152
#, kde-format
msgid "No locations configured"
msgstr ""

#: qml/timer/PresetDurationButton.qml:23 qml/timer/TimerForm.qml:39
#, kde-format
msgid "1 m"
msgstr "1 m"

#: qml/timer/TimerForm.qml:47
#, kde-format
msgid "5 m"
msgstr "5 m"

#: qml/timer/TimerForm.qml:55
#, kde-format
msgid "10 m"
msgstr "10 m"

#: qml/timer/TimerForm.qml:67
#, kde-format
msgid "15 m"
msgstr "15 m"

#: qml/timer/TimerForm.qml:75
#, kde-format
msgid "30 m"
msgstr "30 m"

#: qml/timer/TimerForm.qml:83
#, kde-format
msgid "1 h"
msgstr "1 h"

#: qml/timer/TimerForm.qml:95
#, kde-format
msgid "<b>Duration:</b>"
msgstr "<b>Trvání: </b>"

#: qml/timer/TimerForm.qml:104
#, kde-format
msgid "hours"
msgstr "hodin"

#: qml/timer/TimerForm.qml:115
#, kde-format
msgid "minutes"
msgstr "minut"

#: qml/timer/TimerForm.qml:126
#, kde-format
msgid "seconds"
msgstr "sekund"

#: qml/timer/TimerForm.qml:132
#, kde-format
msgid "<b>Label:</b>"
msgstr "<b>Popisek:</b>"

#: qml/timer/TimerForm.qml:133
#, kde-format
msgid "Timer"
msgstr "Časovač"

#: qml/timer/TimerForm.qml:138
#, kde-format
msgid "<b>Command at timeout:</b>"
msgstr ""

#: qml/timer/TimerForm.qml:141
#, kde-format
msgid "optional"
msgstr "volitelné"

#: qml/timer/TimerForm.qml:157
#, kde-format
msgid "Hide Presets"
msgstr ""

#: qml/timer/TimerForm.qml:157
#, kde-format
msgid "Show Presets"
msgstr "Zobrazit předvolby"

#: qml/timer/TimerForm.qml:162
#, kde-format
msgid "Toggle Delete"
msgstr ""

#: qml/timer/TimerFormWrapper.qml:55
#, kde-format
msgid "<b>Create New Timer</b>"
msgstr ""

#: qml/timer/TimerFormWrapper.qml:73 qml/timer/TimerFormWrapper.qml:132
#, kde-format
msgid "Save As Preset"
msgstr ""

#: qml/timer/TimerFormWrapper.qml:99
#, kde-format
msgid "Create timer"
msgstr ""

#: qml/timer/TimerListDelegate.qml:145 qml/timer/TimerPage.qml:69
#: qml/timer/TimerPage.qml:118
#, kde-format
msgid "Loop Timer"
msgstr ""

#: qml/timer/TimerListPage.qml:27
#, kde-format
msgid "New Timer"
msgstr ""

#: qml/timer/TimerListPage.qml:50
#, kde-format
msgid ""
"The clock daemon was not found. Please start kclockd in order to have timer "
"functionality."
msgstr ""

#: qml/timer/TimerListPage.qml:84
#, kde-format
msgid "No timers configured"
msgstr ""

#: qml/timer/TimerListPage.qml:89
#, kde-format
msgid "Add timer"
msgstr "Přidat časovač"

#: qml/timer/TimerPage.qml:24
#, kde-format
msgid "New timer"
msgstr "Nový časovač"

#: qml/timer/TimerRingingPopup.qml:27
#, kde-format
msgid "Timer has finished"
msgstr "Časovač skončil"

#: qml/timer/TimerRingingPopup.qml:46
#, kde-format
msgid "%1 has completed."
msgstr ""

#: qml/timer/TimerRingingPopup.qml:46
#, kde-format
msgid "The timer has completed."
msgstr ""

#: savedlocationsmodel.cpp:106
#, kde-format
msgid "%1 hour and 30 minutes ahead"
msgstr ""

#: savedlocationsmodel.cpp:106
#, kde-format
msgid "%1 hours and 30 minutes ahead"
msgstr ""

#: savedlocationsmodel.cpp:108
#, kde-format
msgid "%1 hour ahead"
msgstr ""

#: savedlocationsmodel.cpp:108
#, kde-format
msgid "%1 hours ahead"
msgstr ""

#: savedlocationsmodel.cpp:113
#, kde-format
msgid "%1 hour and 30 minutes behind"
msgstr ""

#: savedlocationsmodel.cpp:113
#, kde-format
msgid "%1 hours and 30 minutes behind"
msgstr ""

#: savedlocationsmodel.cpp:115
#, kde-format
msgid "%1 hour behind"
msgstr ""

#: savedlocationsmodel.cpp:115
#, kde-format
msgid "%1 hours behind"
msgstr ""

#: savedlocationsmodel.cpp:118
#, kde-format
msgid "Local time"
msgstr "Místní čas"

#: utilmodel.cpp:72
#, kde-format
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 den"
msgstr[1] "%1 dny"
msgstr[2] "%1 dnů"

#: utilmodel.cpp:76
#, kde-format
msgid ", "
msgstr ", "

#: utilmodel.cpp:78 utilmodel.cpp:84
#, kde-format
msgid " and "
msgstr " a "

#: utilmodel.cpp:80
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 hodina"
msgstr[1] "%1 hodiny"
msgstr[2] "%1 hodin"

#: utilmodel.cpp:86
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuta"
msgstr[1] "%1 minuty"
msgstr[2] "%1 minut"

#: utilmodel.cpp:90
#, kde-format
msgid "Alarm will ring in under a minute"
msgstr ""

#: utilmodel.cpp:92
#, kde-format
msgid "Alarm will ring in %1"
msgstr ""
