#
# Copyright 2020 Han Young <hanyoung@protonmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

# TODO: adapt "org.kde.plasma" here & elsewhere if needed (see README)
add_definitions(-DTRANSLATION_DOMAIN=\"plasma_applet_org.kde.plasma.kclock_1x2\")

set(kclock_1x2_SRCS
    kclock_1x2.cpp
)
add_library(plasma_applet_kclock_1x2 MODULE ${kclock_1x2_SRCS})

target_link_libraries(plasma_applet_kclock_1x2
                      Qt::Gui
                      Qt::DBus
                      KF${QT_MAJOR_VERSION}::Plasma
                      KF${QT_MAJOR_VERSION}::I18n)


install(TARGETS plasma_applet_kclock_1x2 DESTINATION ${KDE_INSTALL_PLUGINDIR}/plasma/applets)
install(FILES kclock_plasmoid_1x2.svg DESTINATION ${KDE_INSTALL_FULL_ICONDIR}/hicolor/scalable/apps/)
plasma_install_package(package org.kde.plasma.kclock_1x2)
